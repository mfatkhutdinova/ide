package answer

type AnswerError struct {
	Message string
}

type AnswerSuccess struct {
	Code    int
	Message string
}
