package main

import (
	"fmt"
	"net"
	"net/http"
)

func GetLocalIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}

	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

func main() {
	ip := GetLocalIP()
	mux := http.NewServeMux()
	mux.HandleFunc("/ip", func(res http.ResponseWriter, req *http.Request) {
		fmt.Fprint(res, ip)
	})

	http.ListenAndServe(":9000", mux)
}
