package main

import (
	httpAnswer "22_ide/homework/pkg/answer"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"time"
)

var accessTime = map[string]map[string]string{}

func main() {
	ip := GetLocalIP()
	accessTime[ip] = make(map[string]string)

	mux := http.NewServeMux()
	mux.HandleFunc("/set-access-time", setAccessTime)
	mux.HandleFunc("/get-first-access-time", getFirstAccessTime)
	mux.HandleFunc("/check-access-time", checkAccessTime)
	http.ListenAndServe(":9000", mux)
}

func GetLocalIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}

	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

func setAccessTime(res http.ResponseWriter, req *http.Request) {
	ip := GetLocalIP()
	accessTime[ip]["lastTime"] = time.Now().Format("02-01-2006 15:04:05")

	if _, ok := accessTime[ip]["firstTime"]; !ok {
		accessTime[ip]["firstTime"] = time.Now().Format("02-01-2006 15:04:05")
	}

	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(200)
	answerSuccess := httpAnswer.AnswerSuccess{
		200, "OK",
	}

	js, err := json.Marshal(answerSuccess)
	if err != nil {
		http.Error(res, err.Error(), http.StatusInternalServerError)
		return
	}
	res.Write(js) //fmt.Fprint(res, "200 OK")
}

func getFirstAccessTime(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(200)

	ip := GetLocalIP()
	if value, ok := accessTime[ip]["firstTime"]; ok {
		fmt.Fprint(res, value)
	} else {
		accessTime[ip]["firstTime"] = time.Now().Format("02-01-2006 15:04:05")
		accessTime[ip]["lastTime"] = time.Now().Format("02-01-2006 15:04:05")

		fmt.Fprint(res, accessTime[ip]["firstTime"])
	}
}

func checkAccessTime(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(200)

	ip := GetLocalIP()
	if value, ok := accessTime[ip]["lastTime"]; ok {
		fmt.Fprint(res, value)
	} else {

		answerError := httpAnswer.AnswerError{
			"No info about this ip: " + ip,
		}

		js, err := json.Marshal(answerError)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		res.Write(js) //fmt.Fprint(res, answerError)

		accessTime[ip]["firstTime"] = time.Now().Format("02-01-2006 15:04:05")
		accessTime[ip]["lastTime"] = time.Now().Format("02-01-2006 15:04:05")
	}
}
