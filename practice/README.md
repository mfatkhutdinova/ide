# Практическое задание к лекции «Редакторы: обзор эффективных для Ozon»

В качестве результата пришлите ссылку на ваш репозиторий в gitlab.

**Важно**: если у вас что-то не получается, то не стесняйтесь задавать вопросы в Slack

# Задание
Нужно разработать http сервер, который принимает запросы от браузера по относительному адресу = `“http://hostname/ip”`.

При открытии этой страницы в браузере должен отобразиться **ip** адрес с которого выполнен запрос.

## Условия
1. Структура проекта должна соответствовать рекомендациям из лекции
2. Запросы с любых других url (например `"/"`, `"/home"`) должны отклоняться с ошибкой **not found** или **bad request**

**Важно**: обратите внимание на работу с **Context** и **ошибками**

## Полезные ссылки
1. Standard Go Project Layout https://github.com/golang-standards/project-layout 
2. Package http https://golang.org/pkg/net/http/
3. Creating a simple “Hello World!” HTTP Server in Go https://medium.com/rungo/creating-a-simple-hello-world-http-server-in-go-31c7fd70466e
4. Package context https://golang.org/pkg/context/